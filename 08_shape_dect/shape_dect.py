#only god knows
import cv2 as cv
import numpy as np
def getContour(canny_img):
    contour,hierr=cv.findContours(canny_img,cv.RETR_EXTERNAL,cv.CHAIN_APPROX_NONE)
    for cnt in contour:
        area=cv.contourArea(cnt)
        #print (area)
        #cv.drawContours(copy_img,cnt,-1,(0,0,0),3)
        perimeter=cv.arcLength(cnt,True)
        #print(perimeter)
        appox=cv.approxPolyDP(cnt,0.03*perimeter,True)
        conner_obj= (len(appox))
        x,y,w,h=cv.boundingRect(appox)
        if(conner_obj==3):
            cv.putText(copy_img,"Triangle",(x+w//2,y+h//2),cv.FONT_HERSHEY_SIMPLEX,1,(0,0,255),2)
            cv.rectangle(copy_img,(x,y),(x+w,h+y),(0,255,0),3)
        if(conner_obj>=8):
            if(0.9<w/h<1.1):
                cv.putText(copy_img,"Circle",(x+w//2,y+h//2),cv.FONT_HERSHEY_SIMPLEX,1,(0,0,255),2)
                cv.rectangle(copy_img,(x,y),(x+w,h+y),(0,255,0),3)
            else: 
                cv.putText(copy_img,"Elipse",(x+w//2,y+h//2),cv.FONT_HERSHEY_SIMPLEX,1,(0,0,2),2)
                cv.rectangle(copy_img,(x,y),(x+w,h+y),(0,255,0),3)

        if(conner_obj==4):
            if(0.9<w/h<1.1):
                cv.putText(copy_img,"Square",(x+w//2,y+h//2),cv.FONT_HERSHEY_SIMPLEX,1,(0,0,255),2)
                cv.rectangle(copy_img,(x,y),(x+w,h+y),(0,255,0),3)
            else: 
                cv.putText(copy_img,"Rectangle",(x+w//2,y+h//2),cv.FONT_HERSHEY_SIMPLEX,1,(0,0,2),2)
                cv.rectangle(copy_img,(x,y),(x+w,h+y),(0,255,0),3)

        if(conner_obj==5):
            cv.putText(copy_img,"pentagon",(x+w//2,y+h//2),cv.FONT_HERSHEY_PLAIN,1,(0,0,255),2)
            cv.rectangle(copy_img,(x,y),(x+w,h+y),(0,255,0),3)
        if(conner_obj==6):
            cv.putText(copy_img,"Hexgon",(x+w//2,y+h//2),cv.FONT_HERSHEY_PLAIN,1,(0,0,255),2)
            cv.rectangle(copy_img,(x,y),(x+w,h+y),(0,255,0),3)



shape_img=cv.imread("shapes.jpg")
copy_img=shape_img.copy()
Grey_img=cv.cvtColor(shape_img,cv.COLOR_BGR2GRAY)
Blur_img=cv.GaussianBlur(Grey_img,(9,9),1)
canny_img=cv.Canny(Blur_img,50,50)
getContour(canny_img)
cv.imshow("Blur",copy_img)
cv.waitKey(0)
