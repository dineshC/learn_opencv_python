import cv2 as cv
import numpy as np

my_color=[[68,107,0,179,223,225],
        [0,150,0,16,197,248]
        ]

def color_find(img):
    HSV_img=cv.cvtColor(img,cv.COLOR_BGR2HSV)
    for color in my_color :
        lower =np.array(color[0:3])
        higher=np.array(color[3:])
        mask=cv.inRange(HSV_img,lower,higher)
        cv.imshow(str(color[0]),mask)


video_cam=cv.VideoCapture(0)
frame_height=480
frame_width=640
video_cam.set(10,110)
video_cam.set(3,frame_width)
video_cam.set(4,frame_height)

while True:
    success,img_ff=video_cam.read()
    color_find(img_ff)
    cv.imshow("WEB CAM",img_ff)
    if(cv.waitKey(1) & 0xff==ord('q')):
        break

