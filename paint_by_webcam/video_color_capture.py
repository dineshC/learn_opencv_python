import numpy as np
import cv2 as cv
def empty():
    pass

cv.namedWindow("Track ball")
cv.resizeWindow("Track ball",320,480)
cv.createTrackbar("Hue_min","Track ball",0,179,empty)
cv.createTrackbar("Hue_max","Track ball",179,179,empty)
cv.createTrackbar("Sat_min","Track ball",0,255,empty)
cv.createTrackbar("Sat_max","Track ball",255,255,empty)
cv.createTrackbar("val_min","Track ball",0,255,empty)
cv.createTrackbar("val_max","Track ball",255,255,empty)

video_capture=cv.VideoCapture(0)
video_capture.set(10,130)
while True:
    success,img_ff=video_capture.read()
    cv.imshow("video",img_ff)
    img_HSV=cv.cvtColor(img_ff,cv.COLOR_BGR2HSV)
    h_min=cv.getTrackbarPos("Hue_min","Track ball")
    h_max=cv.getTrackbarPos("Hue_max","Track ball")
    s_min=cv.getTrackbarPos("Sat_min","Track ball")
    s_max=cv.getTrackbarPos("Sat_max","Track ball")
    v_min=cv.getTrackbarPos("val_min","Track ball")
    v_max=cv.getTrackbarPos("val_max","Track ball")
    #print(h_min,h_max,s_min,s_max,v_min,v_min)
    lower=np.array([h_min,s_min,v_min])
    higher=np.array([h_max,s_max,v_max])
    mask=cv.inRange(img_HSV,lower,higher)
    img_Result=cv.bitwise_and(img_ff,img_ff,mask=mask)
    cv.imshow("video_hsv",img_Result)


    if cv.waitKey(1) & 0xff==ord('q'):
        break



