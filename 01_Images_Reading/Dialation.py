import cv2 as cv
import numpy as np

kernal=np.ones((5,5),np.uint8)
color_img=cv.imread("01.png")
canny_img=cv.Canny(color_img,100,100)
#diluted_img
diluted_img=cv.dilate(canny_img,kernal,iterations=1)
#eroded
eroded_img=cv.erode(diluted_img,kernal,iterations=1)

cv.imshow('diluted image',eroded_img)
cv.waitKey(10000)

