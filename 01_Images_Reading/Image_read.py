import cv2 as cv
import sys
#read image
img=cv.imread("01.png")
#check
if img is None:
    sys.exit("No image found")
#show
cv.imshow("Image view as sample",img)

#Save when s is pressed
save_img=cv.waitKey(0)
if save_img==ord("s"):
    cv.imwrite("Capture_image.jpg",img)

