import cv2 as cv
import numpy as np
#open web cam
web_cap=cv.VideoCapture(0)
#format MP4
fourcc=cv.VideoWriter_fourcc('M','J','P','G')
#file output.mp4,encode,fps,(wid,hight)
out=cv.VideoWriter('output.mp4',fourcc,30.0,(640,480))

while True:
    #capture frame by frame image
    bool_video,frame_cap=web_cap.read()
    #check bool value img is captured
    if not bool_video:
        print("web cam not found")
        break
#write in output file
    out.write(frame_cap)
    cv.imshow('output',frame_cap)
    #Press 'q' for quit
    if cv.waitKey(1)==ord('q'):
        out.release()
        break
#destroy all task
web_cap.release()
cv.destroyAllWindows()

