import cv2 as cv
import numpy as np

face_cascase=cv.CascadeClassifier("haarcascade_frontalface.xml")
img_=cv.imread("01.jpg")
grey_img=cv.cvtColor(img_,cv.COLOR_BGR2GRAY)

face=face_cascase.detectMultiScale(grey_img,1.1,4)

for (x,y,w,h) in face:
    cv.rectangle(img_,(x,y),(x+h,y+h),(255,255,250),3)

cv.imshow("Face",img_)
cv.waitKey(0)





