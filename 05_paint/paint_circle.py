import numpy as np
import cv2 as cv

def draw_circle(event,x,y,flags,param):
    if event==cv.EVENT_FLAG_LBUTTON:
        cv.circle(img,(x,y),10,(255,0,255),-1)

img=np.zeros((512,512,3),np.uint8)
cv.namedWindow('draw')
cv.setMouseCallback('draw',draw_circle)

while True:
    cv.imshow('draw',img)
    if (cv.waitKey(1)) & 0xFF==ord('q'):
        break
cv.destroyAllWindows()
