import cv2 as cv
import numpy as np
#web cam open
web_capture=cv.VideoCapture(0)
#name for image start 1
i=0

while True:
#frame by frame image
    ret,frame=web_capture.read()
    
    if not ret:
        print("Can't receive frame (stream end?). Exiting ...")
        break
    #change BGR(default) to rgb
    RBG=cv.cvtColor(frame,cv.COLOR_BGR2RGB)
    #show rgb
    cv.imshow("Video",RBG)
    #show BDR
    cv.imshow("nature",frame)
    
    k=cv.waitKey(1)
    
    if k==ord("s"):
        i+=1
#save image as RGB
        cv.imwrite(str(i)+".jpg",RBG)
   
    q=cv.waitKey(1)
#q to quit
    if ord("q")==q:
        break
web_capture.release()
cv.destroyAllWindows()
