import numpy as np
import cv2 as cv
# Create a black image
#eg
#numpy	    output
#np.zero(3)=>[0.0,0.0,0.0]
#np.zero(3,unint8)=>[0,0,0]
#create 
img = np.zeros((512,512,3),np.uint8)
#print(img)
'''

[[[0 0 0]
  [0 0 0]
  [0 0 0]
  ...
  [0 0 0]
  [0 0 0]
  [0 0 0]]

 [[0 0 0]
  [0 0 0]
  [0 0 0]
  ...
  [0 0 0]
  [0 0 0]
  [0 0 0]]

 [[0 0 0]
  [0 0 0]
  [0 0 0]
  ...
  [0 0 0]
  [0 0 0]
  [0 0 0]]

 ...

 [[0 0 0]
  [0 0 0]
  [0 0 0]
  ...
  [0 0 0]
  [0 0 0]
  [0 0 0]]

 [[0 0 0]
  [0 0 0]
  [0 0 0]
  ...
  [0 0 0]
  [0 0 0]
  [0 0 0]]

 [[0 0 0]
  [0 0 0]
  [0 0 0]
  ...
  [0 0 0]
  [0 0 0]
  [0 0 0]]]



'''



Line_image=cv.line(img,(0,0),(200,500),(255,255,255),12)
cv.imshow('line',Line_image)

cv.waitKey(1000)
cv.destroyAllWindows

